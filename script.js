const canvas = document.getElementById('animationCanvas');
const ctx = canvas.getContext('2d');
canvas.width = 800;
canvas.height = 600;

const flowers = [
    { x: 100, y: 250, color: 'red', text: "Loving", revealed: false },
    { x: 200, y: 400, color: 'yellow', text: "Brilliant", revealed: false },
    { x: 400, y: 200, color: 'blue', text: "Inspirational", revealed: false },
    { x: 600, y: 450, color: 'purple', text: "Amazing", revealed: false },
    { x: 700, y: 300, color: 'orange', text: "Selfless", revealed: false }
];
let currentFlower = 0;
let grassPositions = []; // Store positions for grass
let wingFlap = 0; // Wing animation state

function generateGrass() {
    for (let i = 0; i < 50; i++) {
        let x = Math.random() * canvas.width;
        let y = canvas.height - Math.random() * 100; // Grass at the bottom
        let height = Math.random() * 20 + 20;
        let angle = Math.PI / 4 * Math.random() - Math.PI / 8;
        grassPositions.push({x, y, height, angle});
    }
}

function drawGrass() {
    grassPositions.forEach(grass => {
        ctx.strokeStyle = 'darkgreen';
        ctx.lineWidth = 2;
        ctx.beginPath();
        ctx.moveTo(grass.x, grass.y);
        ctx.lineTo(grass.x + Math.sin(grass.angle) * grass.height, grass.y - grass.height);
        ctx.stroke();
    });
}

function drawFlower(flower) {
    ctx.fillStyle = flower.color;
    // Draw petals
    for (let i = 0; i < 5; i++) {
        ctx.beginPath();
        ctx.ellipse(flower.x, flower.y, 50, 25, Math.PI / 5 * i, 0, Math.PI * 2);
        ctx.fill();
    }
    // Draw center
    ctx.fillStyle = flower.color === 'yellow' ? 'white' : 'yellow'; // Change center color for yellow flower
    ctx.beginPath();
    ctx.arc(flower.x, flower.y, 12, 0, Math.PI * 2);
    ctx.fill();
    // Draw stem
    ctx.strokeStyle = 'green';
    ctx.lineWidth = 10;
    ctx.beginPath();
    ctx.moveTo(flower.x, flower.y + 12);
    ctx.lineTo(flower.x, flower.y + 150);
    ctx.stroke();

    if (flower.revealed) {
        ctx.fillStyle = 'black';
        ctx.font = 'bold 16px Arial';
        ctx.fillText(flower.text, flower.x - ctx.measureText(flower.text).width / 2, flower.y - 40);
    }
}

function drawButterfly(x, y) {
    // Wings
    const colors = ['magenta', 'cyan', 'lime', 'gold'];
    colors.forEach((color, index) => {
        ctx.fillStyle = color;
        let wingY = y - (index < 2 ? 10 + wingFlap : -10 - wingFlap);
        ctx.beginPath();
        ctx.ellipse(x + (index % 2 ? 20 : -20), wingY, 16, 12, Math.PI / 4, 0, Math.PI * 2);
        ctx.fill();
    });
    // Body
    ctx.fillStyle = 'black';
    ctx.beginPath();
    ctx.ellipse(x, y, 5, 15, 0, 0, Math.PI * 2); // More detailed elliptical body
    ctx.fill();
    // Antennae
    ctx.strokeStyle = 'black';
    ctx.lineWidth = 1;
    ctx.beginPath();
    ctx.moveTo(x - 2, y - 15);
    ctx.lineTo(x - 10, y - 25);
    ctx.moveTo(x + 2, y - 15);
    ctx.lineTo(x + 10, y - 25);
    ctx.stroke();
}


function animateButterfly() {
    if (currentFlower >= flowers.length) {
        render(); // Ensure the final scene is rendered before adding the message
        ctx.fillStyle = 'black';
        ctx.font = 'bold 32px Arial';
        ctx.fillText("Happy Mother's Day!", canvas.width / 2 - ctx.measureText("Happy Mother's Day!").width / 2, 80); // Moved up
        ctx.font = 'bold 24px Arial'; // Smaller font size for the additional text
        ctx.fillText("Thank you for all you do!", canvas.width / 2 - ctx.measureText("Thank you for all you do!").width / 2, 120); // New message added
        return;
    }
    let flower = flowers[currentFlower];
    let targetX = flower.x;
    let targetY = flower.y; // Center the butterfly over the flower
    let dx = (targetX - butterfly.x) / 100;
    let dy = (targetY - butterfly.y) / 100;

    function frame() {
        if (Math.abs(butterfly.x - targetX) < 1 && Math.abs(butterfly.y - targetY) < 1) {
            flower.revealed = true;
            wingFlap = 0; // Reset wing position when landing
            render(); // Update to keep the text
            currentFlower++;
            setTimeout(animateButterfly, 1500); // Wait a bit longer before moving to the next flower
        } else {
            butterfly.x += dx;
            butterfly.y += dy;
            wingFlap = (wingFlap + 1) % 10; // Simulate wing flapping
            render();
            requestAnimationFrame(frame);
        }
    }
    frame();
}

function render() {
    ctx.clearRect(0, 0, canvas.width, canvas.height);
    ctx.fillStyle = '#7bbf6a';  // Redraw the grassy background
    ctx.fillRect(0, 0, canvas.width, canvas.height);
    drawGrass();
    flowers.forEach(drawFlower);
    drawButterfly(butterfly.x, butterfly.y);
}

const butterfly = { x: 50, y: 50 };
generateGrass(); // Generate grass only once
flowers.forEach(drawFlower);
animateButterfly();
